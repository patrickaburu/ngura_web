<?php
use App\Beer;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', function () {
    return view('auth.login');
});


Route::get('/home', 'HomeController@index');
Auth::routes();


 Route::group(['middleware'=>'auth'],function()
{

        Route::get('business', function () {
            return view('business');
        });
Route::get('home/beer', 'BeerController@index');
Route::get('view', 'BeerController@view');
Route::get('soda', 'SodaController@view');
Route::get('wine', 'WineController@view');
Route::get('repo', 'ReportController@mash');

Route::get('/ajax-subcat', 'BeerController@ajax');
Route::get('/report', 'ReportController@report');

Route::post('make/sell',  'BeerController@create');
Route::post('sale/wine',  'WineController@create');
Route::post('sale/soda',  'SodaController@create');
Route::get('spirits',  'SpiritsController@view');
Route::post('sell/spirits',  'SpiritsController@create');

    Route::get('sms',  'Sendsms@index');
    Route::post('send/sms',  'Sendsms@sms');



});
//admin routes
Route::group(['middleware'=>'admin'],function()
{
    Route::get('admim', function () {
        return view('wrap');
    });

    Route::get('admin/home', 'Adminreport@report');

    Route::get('admin/products', 'Adminreport@view');
    Route::post('product/update/{id}', 'Adminreport@update');
    Route::post('admin/add/product', 'Adminreport@insert');
    Route::post('admin/add/stock/{id}', 'Adminreport@stock');

});