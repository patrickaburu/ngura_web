<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

//login
Route::post('v1/login','ApiController@login');

//sms
Route::get('v1/sms','ApiController@sms');

//statements
Route::get('v1/statement','ApiController@statements');

//total sales
Route::post('v1/totalsales','ApiController@totalSale');

//REGISTER USER
Route::post('v1/register','ApiController@register');
//add product
Route::post('v1/addproduct','ApiController@addproduct');
//get remaining stock
Route::get('v1/stock','ApiController@remainingStock');