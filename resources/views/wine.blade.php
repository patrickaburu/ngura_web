@extends('layouts.app')
<style>
    #here{
        background-color: black!important;
    }
</style>
@section('content')
    <header class="page-header">

    </header>
    <section class="col-lg-2">
        @include('partials/home')
    </section>
    <section class="col-lg-8">

        <div class="panel panel-inverse " >
            <div class="panel-heading" style="background-color:black" >
                <h2 style="color: white"> Enter today sale</h2>

            </div>

            <table class="table w3-bordered w3-border w3-table w3-striped" style="font-size: 12px;">
                <th><span class="glyphicon glyphicon-tasks"></span> id</th>
                <th><span class="glyphicon glyphicon-edit"></span> name</th>
                <th><span class="glyphicon glyphicon-map-marker"></span> price</th>
                <th colspan="3"><span></span> Action</th>
                @foreach($data as $content)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $content->name  }}</td>
                        <td>{{ $content->price  }}</td>

                        <td><a data-toggle="modal" data-target="#myModalEdit{{ $content->id }}" href="{{ $content->id  }}"><span class="glyphicon glyphicon-edit">sell</span></a> </td>

                    </tr>



                    <!-- Button trigger modal -->
                    {{--action="{{ URL::to('make_sell')  }}"--}}

                <!-- Modal for sales -->
                    <div class="modal fade" id="myModalEdit{{ $content->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

                        {{--<?php $edit_site = App\Beer::where('id',$content->id)->get(); ?>--}}
                        {{--@foreach($edit_site as $item)--}}
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Enter your day sell</h4>
                                </div>
                                <div class="modal-body">
                                    <form  action="{{ URL::to('sale/wine')  }}" method="post" autocomplete="off">
                                        {{ csrf_field() }}

                                        <div class="form-group" id="" >

                                            <input type="button" class="form-control" name="id" value="{{ $content->name  }}">
                                            <input type="hidden" name="product" value="{{$content->id}}">
                                            <input type="hidden" name="price" value="{{$content->price}}">
                                        </div>


                                        <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                            <label for="name" class="control-label">Quantity</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                                                <input type="text" class="form-control" name="quantity" value="{{ old('quantity')  }}">
                                            </div>
                                            @if ($errors->has('quantity'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>



                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"  >Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>

                        {{--@endforeach--}}
                    </div>

                @endforeach

            </table>
            @if (session('status'))
                <div class="alert alert-warning">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
                </div>
    @endif

@endsection