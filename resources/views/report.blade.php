@extends('layouts.app')
<style>
    #here{
        background-color: black!important;
    }
</style>
@section('content')
    <header class="page-header">

    </header>
    <section class="col-lg-2">
        @include('partials/home')
    </section>
    <section class="col-lg-8">

        <div class="panel panel-inverse " >
            <div class="panel-heading" style="background-color:black" >
                <h2 style="color: white">Today Sale report for {{ Auth::user()->name }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kshs:{{$total}}</h2>

            </div>


            <table class="table w3-bordered w3-border w3-table w3-striped" style="font-size: 12px;">
                <th><span class="glyphicon glyphicon-tasks"></span> number</th>
                <th><span class="glyphicon glyphicon-edit"></span> name</th>
                <th><span class="glyphicon glyphicon-edit"></span> Quantity</th>
                <th><span class="glyphicon glyphicon-map-marker"></span> total</th>




                @foreach($data as $content)

                    <tr>
                        <td>{{ $loop->iteration }}</td>
                         <td>{{$content->name}} </td>

                        <td>{{ $content->quantity  }}</td>
                        <td>{{ $content->total  }}</td>

                    </tr>

                @endforeach


            </table>

@endsection