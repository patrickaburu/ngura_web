@if(Auth::user()->role==2)
<ul class="sidebar-menu">
    <li class="header"><a href=""> Bar mgt</a></li>
    <li><a class="text-success" href="{{ URL::to('view')  }}" ><span class="fa fa-plus-circle"></span>  Beer</a> </li>

    <li><a class="text-danger"href="{{ URL::to('soda')  }}" ><span class="fa fa-minus-circle"></span> soda</a> </li>
    <li><a class="text-danger" href="{{ URL::to('spirits')  }}" ><span class="fa fa-minus-circle"></span> Spirits</a> </li>
    <li><a class="text-danger" href="{{ URL::to('wine')  }}" ><span class="fa fa-minus-circle"></span> Wines</a> </li>
    <li><a class="text-danger" href="{{URL::to('report')}}" ><span class="fa fa-minus-circle"></span> Total Sales</a> </li>
    <li><a class="text-danger" href="{{URL::to('report')}}" ><span class="fa fa-minus-circle"></span> Expense and debt</a> </li>
    <li><a class="text-danger" href="{{URL::to('sms')}}" ><span class="fa fa-minus-circle"></span>Contact Manager</a> </li>

</ul>
  @elseif(Auth::user()->role==1)
    <ul class="sidebar-menu">
        <li class="header"><a href="">Admin Dashborad</a></li>
        <li><a class="text-success" href="{{ URL::to('admin/home')  }}" ><span class="fa fa-plus-circle"></span>  Total sales</a> </li>
        <li><a class="text-muted" href="{{ URL::to('admin/products')  }}" ><span class="fa fa-eye"></span>Products</a> </li>
        <li><a class="text-danger" href="/register" ><span class="fa fa-minus-circle"></span> Users</a> </li>

    </ul>
 @endif