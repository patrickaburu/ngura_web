@extends('layouts.app')
<style>
    #here{
        background-color: black!important;
    }
</style>
@section('content')
    <header class="page-header">

    </header>
    <section class="col-lg-2">
        @include('partials/home')
    </section>
    <section class="col-lg-8">

        <div class="panel panel-inverse " >
            <div class="panel-heading" style="background-color:black" >
                <h2 style="color: white"> Products</h2>

            </div>




    <table>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/add/product') }}">
            {{ csrf_field() }}
        <thead>
        <tr>
            <th>Name</th>
            <th>Product Category</th>
            <th>price</th>
            <th >Action </th>

        </tr>
        </thead>
        <tbody>
        <tr>
            <td><input type="text" class="form-control"  name="name" placeholder="name of the product"> </td>
            <td>  <select class="form-control" name="select">
                    <option value="" disabled selected>Choose product category</option>
                    <option value="1">Beer</option>
                    <option value="3">spirits</option>
                    <option value="2">wine</option>
                    <option value="4">soda</option>
                </select>
                </td>
            <td><input type="text" class="form-control" name="price" placeholder="price of the product"></td>
            <td><button type="submit" class="btn btn-primary">Add</button> </td>
        </tr>
        </tbody>
            </form>
    </table>



            {{--// module bootstrap--}}


{{--end of module --}}

                            <div class="row">
                <div class="col s12" >

                    <ul class="tabs">
                        <li class="tab col s3"><a href="#test1" name ="cat" value="1">Beer</a></li>
                        <li class="tab col s3"><a  href="#test2" name="catt" value="2">Spirits</a></li>
                        <li class="tab col s3 "><a href="#test3">wine</a></li>
                        <li class="tab col s3"><a href="#test4">soda</a></li>
                    </ul>
                </div>

                                {{--start test 1--}}
                <div id="test1" class="col s12">
                    <table class="table w3-bordered w3-border w3-table w3-striped" style="font-size: 12px;">
                        <th><span class="glyphicon glyphicon-tasks"></span> id</th>
                        <th><span class="glyphicon glyphicon-edit"></span> name</th>
                        <th><span class="glyphicon glyphicon-map-marker"></span> stock</th>
                        <th><span class="glyphicon glyphicon-map-marker"></span> price</th>
                        <th><span class="glyphicon glyphicon-map-marker"></span> Action</th>
                        <th><span class="glyphicon glyphicon-map-marker"></span> Action</th>
                        {{--<th colspan="3"><span></span> Action</th>--}}
                        @foreach($beer as $content)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $content->product_name  }}</td>
                                <td>{{ $content->quantity  }}</td>
                                <td>{{ $content->price  }}</td>
                                <td><a data-toggle="modal" data-target="#myModalEdit{{ $content->id }}" href="{{ $content->id  }}"><span class="glyphicon glyphicon-edit">Change Price</span></a> </td>
                                <td><a data-toggle="modal" data-target="#myModalStock{{ $content->id }}" href="{{ $content->id  }}"><span class="glyphicon glyphicon-edit">Add stock</span></a> </td>


                            </tr>
                            <div class="modal fade" id="myModalEdit{{ $content->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

                                {{--<?php $edit_site = App\Beer::where('id',$content->id)->get(); ?>--}}
                                {{--@foreach($edit_site as $item)--}}
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Change Price</h4>
                                        </div>
                                        <div class="modal-body">

                                            <form  action="{{ URL::to('product/update',$content->id)  }}" method="post" autocomplete="off">
                                                {{ csrf_field() }}

                                                <div class="form-group" id="" >

                                                    <input type="button" class="form-control" name="id" value="{{ $content->product_name }}">
                                                    <input type="hidden" name="product" value="{{$content->id}}">
                                                    <input type="hidden" name="price" value="{{$content->price}}">
                                                </div>


                                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                                    <label for="name" class="control-label">Price</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                                                        <input type="text" class="form-control" name="price" value="{{ old('quantity')  }}">
                                                    </div>
                                                    @if ($errors->has('quantity'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                                    @endif
                                                </div>



                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary"  >Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                </div>
                        {{--end of price modal--}}
                            {{--stock modal--}}
                            <div class="modal fade" id="myModalStock{{ $content->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">


                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Add stock</h4>
                                        </div>
                                        <div class="modal-body">

                                            <form  action="{{ URL::to('admin/add/stock',$content->id)  }}" method="post" autocomplete="off">
                                                {{ csrf_field() }}

                                                <div class="form-group" id="" >

                                                    <input type="button" class="form-control" name="id" value="{{ $content->product_name }}">
                                                    <input type="hidden" name="id" value="{{$content->id}}">
                                                    {{--<input type="hidden" name="price" value="{{$content->price}}">--}}
                                                </div>


                                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                                    <label for="name" class="control-label">Stock</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                                                        <input type="text" class="form-control" name="price" value="{{ old('quantity')  }}">
                                                    </div>
                                                    @if ($errors->has('quantity'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                                    @endif
                                                </div>



                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary"  >Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            {{--end of stock modal--}}
                    @endforeach
                    </table>
                </div>

                                {{--start test 2--}}
                                <div id="test2" class="col s12">
                                    <table class="table w3-bordered w3-border w3-table w3-striped" style="font-size: 12px;">
                                        <th><span class="glyphicon glyphicon-tasks"></span> id</th>
                                        <th><span class="glyphicon glyphicon-edit"></span> name</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> stock</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> price</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> Action</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> Action</th>

                                        @foreach($spirits as $content)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $content->product_name  }}</td>
                                                <td>{{ $content->quantity  }}</td>
                                                <td>{{ $content->price  }}</td>
                                                <td><a data-toggle="modal" data-target="#myModalEdit{{ $content->id }}" href="{{ $content->id  }}"><span class="glyphicon glyphicon-edit">Change Price</span></a> </td>
                                                <td><a data-toggle="modal" data-target="#myModalStock{{ $content->id }}" href="{{ $content->id  }}"><span class="glyphicon glyphicon-edit">Add stock</span></a> </td>


                                            </tr>
                                            <div class="modal fade" id="myModalEdit{{ $content->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

                                                {{--<?php $edit_site = App\Beer::where('id',$content->id)->get(); ?>--}}
                                                {{--@foreach($edit_site as $item)--}}
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Change Price</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <form  action="{{ URL::to('product/update',$content->id)  }}" method="post" autocomplete="off">
                                                                {{ csrf_field() }}

                                                                <div class="form-group" id="" >

                                                                    <input type="button" class="form-control" name="id" value="{{ $content->product_name }}">
                                                                    <input type="hidden" name="product" value="{{$content->id}}">
                                                                    <input type="hidden" name="price" value="{{$content->price}}">
                                                                </div>


                                                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                                                    <label for="name" class="control-label">Price</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                                                                        <input type="text" class="form-control" name="price" value="{{ old('quantity')  }}">
                                                                    </div>
                                                                    @if ($errors->has('quantity'))
                                                                        <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                                                    @endif
                                                                </div>



                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary"  >Save changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--end of price modal--}}
                                            {{--stock modal--}}
                                            <div class="modal fade" id="myModalStock{{ $content->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">


                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Add stock</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <form  action="{{ URL::to('admin/add/stock',$content->id)  }}" method="post" autocomplete="off">
                                                                {{ csrf_field() }}

                                                                <div class="form-group" id="" >

                                                                    <input type="button" class="form-control" name="id" value="{{ $content->product_name }}">
                                                                    <input type="hidden" name="id" value="{{$content->id}}">
                                                                    {{--<input type="hidden" name="price" value="{{$content->price}}">--}}
                                                                </div>


                                                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                                                    <label for="name" class="control-label">Stock</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                                                                        <input type="text" class="form-control" name="price" value="{{ old('quantity')  }}">
                                                                    </div>
                                                                    @if ($errors->has('quantity'))
                                                                        <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                                                    @endif
                                                                </div>



                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary"  >Save changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--end of stock modal--}}
                                        @endforeach
                                    </table>
                                </div>
               {{--start test 3--}}

                                <div id="test3" class="col s12">
                                    <table class="table w3-bordered w3-border w3-table w3-striped" style="font-size: 12px;">
                                        <th><span class="glyphicon glyphicon-tasks"></span> id</th>
                                        <th><span class="glyphicon glyphicon-edit"></span> name</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> stock</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> price</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> action</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> action</th>

                                        @foreach($wine as $content)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $content->product_name  }}</td>
                                                <td>{{ $content->quantity  }}</td>
                                                <td>{{ $content->price  }}</td>
                                                <td><a data-toggle="modal" data-target="#myModalEdit{{ $content->id }}" href="{{ $content->id  }}"><span class="glyphicon glyphicon-edit">Change Price</span></a> </td>
                                                <td><a data-toggle="modal" data-target="#myModalStock{{ $content->id }}" href="{{ $content->id  }}"><span class="glyphicon glyphicon-edit">Add stock</span></a> </td>


                                            </tr>
                                            <div class="modal fade" id="myModalEdit{{ $content->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

                                                {{--<?php $edit_site = App\Beer::where('id',$content->id)->get(); ?>--}}
                                                {{--@foreach($edit_site as $item)--}}
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Change Price</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <form  action="{{ URL::to('product/update',$content->id)  }}" method="post" autocomplete="off">
                                                                {{ csrf_field() }}

                                                                <div class="form-group" id="" >

                                                                    <input type="button" class="form-control" name="id" value="{{ $content->product_name }}">
                                                                    <input type="hidden" name="product" value="{{$content->id}}">
                                                                    <input type="hidden" name="price" value="{{$content->price}}">
                                                                </div>


                                                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                                                    <label for="name" class="control-label">Price</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                                                                        <input type="text" class="form-control" name="price" value="{{ old('quantity')  }}">
                                                                    </div>
                                                                    @if ($errors->has('quantity'))
                                                                        <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                                                    @endif
                                                                </div>



                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary"  >Save changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--end of price modal--}}
                                            {{--stock modal--}}
                                            <div class="modal fade" id="myModalStock{{ $content->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">


                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Add stock</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <form  action="{{ URL::to('admin/add/stock',$content->id)  }}" method="post" autocomplete="off">
                                                                {{ csrf_field() }}

                                                                <div class="form-group" id="" >

                                                                    <input type="button" class="form-control" name="id" value="{{ $content->product_name }}">
                                                                    <input type="hidden" name="id" value="{{$content->id}}">
                                                                    {{--<input type="hidden" name="price" value="{{$content->price}}">--}}
                                                                </div>


                                                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                                                    <label for="name" class="control-label">Stock</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                                                                        <input type="text" class="form-control" name="price" value="{{ old('quantity')  }}">
                                                                    </div>
                                                                    @if ($errors->has('quantity'))
                                                                        <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                                                    @endif
                                                                </div>



                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary"  >Save changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--end of stock modal--}}
                                        @endforeach
                                    </table>
                                </div>

                                {{--stop test 3--}}
                        {{--test 4--}}
                                <div id="test4" class="col s12">
                                    <table class="table w3-bordered w3-border w3-table w3-striped" style="font-size: 12px;">
                                        <th><span class="glyphicon glyphicon-tasks"></span> id</th>
                                        <th><span class="glyphicon glyphicon-edit"></span> name</th>
                                        <th><span class="glyphicon glyphicon-edit"></span> stock</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> price</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> action</th>
                                        <th><span class="glyphicon glyphicon-map-marker"></span> action</th>

                                        @foreach($soda as $content)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $content->product_name  }}</td>
                                                <td>{{ $content->quantity  }}</td>
                                                <td>{{ $content->price  }}</td>

                                                <td><a data-toggle="modal" data-target="#myModalEdit{{ $content->id }}" href="{{ $content->id  }}"><span class="glyphicon glyphicon-edit">Change Price</span></a> </td>
                                                <td><a data-toggle="modal" data-target="#myModalStock{{ $content->id }}" href="{{ $content->id  }}"><span class="glyphicon glyphicon-edit">Add stock</span></a> </td>

                                                {{--start price--}}
                                                <div class="modal fade" id="myModalEdit{{ $content->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

                                                    {{--<?php $edit_site = App\Beer::where('id',$content->id)->get(); ?>--}}
                                                    {{--@foreach($edit_site as $item)--}}
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Change Price</h4>
                                                            </div>
                                                            <div class="modal-body">

                                                                <form  action="{{ URL::to('product/update',$content->id)  }}" method="post" autocomplete="off">
                                                                    {{ csrf_field() }}

                                                                    <div class="form-group" id="" >

                                                                        <input type="button" class="form-control" name="id" value="{{ $content->product_name }}">
                                                                        <input type="hidden" name="product" value="{{$content->id}}">
                                                                        <input type="hidden" name="price" value="{{$content->price}}">
                                                                    </div>


                                                                    <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                                                        <label for="name" class="control-label">Price</label>
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                                                                            <input type="text" class="form-control" name="price" value="{{ old('quantity')  }}">
                                                                        </div>
                                                                        @if ($errors->has('quantity'))
                                                                            <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                                                        @endif
                                                                    </div>



                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary"  >Save changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--stop price--}}
                                            </tr>

                                            {{--stock start--}}
                                            <div class="modal fade" id="myModalStock{{ $content->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">


                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Add stock</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <form  action="{{ URL::to('admin/add/stock',$content->id)  }}" method="post" autocomplete="off">
                                                                {{ csrf_field() }}

                                                                <div class="form-group" id="" >

                                                                    <input type="button" class="form-control" name="id" value="{{ $content->product_name }}">
                                                                    <input type="hidden" name="id" value="{{$content->id}}">
                                                                    {{--<input type="hidden" name="price" value="{{$content->price}}">--}}
                                                                </div>


                                                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                                                    <label for="name" class="control-label">stock</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                                                                        <input type="text" class="form-control" name="price" value="{{ old('quantity')  }}">
                                                                    </div>
                                                                    @if ($errors->has('quantity'))
                                                                        <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                                                    @endif
                                                                </div>



                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary"  >Save changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--stock stop--}}

                                    @endforeach
                                    </table>
                                </div>
                        {{--stop test 4--}}

<script>

    $(document).ready(function(){
        $('ul.tabs').tabs('select_tab', 'tab_id');
    });

</script>

@endsection