@extends('layouts.app')


@section('content')
<section class="container">
    <header class="page-header">
        Add a category
    </header>
    <section class="col-lg-2">
        @include('partials/home')
    </section>
    <section class="col-lg-8">
           
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3> Add a category</h3>
            </div>
            <div class="panel-body">

             <!--    {{--form --}} -->
                    <form action="{{ URL::to('advert/post_category')  }}" method="post" accept-charset="utf-8" autocomplete="off">
                        {{  csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Tusker</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Plisner</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">White Cap</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Balozi</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Allsopes</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Snaps</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Guinness</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Tusker Lite</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Whitecap Lite</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Tusker Malt</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Black Ice</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Castle</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name')  }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">save category</button>
                        </div>
                    </form>
               <!--  {{--end form--}} -->
            </div>
        </div>
    </section>
</section>

    @endsection