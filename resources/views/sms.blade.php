@extends('layouts.app')
<style>
    #here{
        background-color: black!important;
    }
</style>
<div class="panel panel-inverse " >
@section('content')
    <header class="page-header">

    </header>
    <section class="col-lg-2">
        @include('partials/home')
    </section>
        <section class="col-lg-8">

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/send/sms') }}">
        {{ csrf_field() }}
            <input type="text" class="form-control"  name="txt" placeholder="Type Your message ">
            <button type="submit" class="btn btn-primary">Send Message</button>
    </form>
    </section>


@endsection
    @if (session('status'))
        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
    @endif
</div>