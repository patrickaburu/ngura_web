<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Support\Facades\Auth;
use App\Sale;
use App\Stock;
use DB;
use Illuminate\Support\Facades\Response;
use carbon;
use Illuminate\Http\Request;

class adminreport extends Controller
{
    public function report(){
        $mytime = \Carbon\Carbon::today();

        $date= $mytime->toDateString();

        $results = DB::table('sales')
            ->join('products','sales.product_id','=','products.id')
            ->join('categories','products.category_id','=','categories.id')
            ->join('users','sales.users_id','=','users.id')
            ->select(DB::raw('products.name, SUM(sales.quantity) as quantity, SUM(sales.total) as total,users.name as User_name'))
            ->where([['sales.sale_date',$date]])

            ->groupBy('users.name')
            ->groupBy('sales.product_id')
            ->groupBy('products.name')->get();

//        $total=DB::table('sales')
        $report = DB::table('sales')->where([

            ['sale_date', '=', $date],
        ])->get();




        $sum=$report->sum('total');

        return view('admin/home',['data'=>$results] ,['total'=>$sum]);


    }
    public function  view(Request $request){




        $Beer = DB::table('stocks')
            ->join('products','stocks.product_id','=','products.id')
            ->join('categories','products.category_id','=','categories.id')
            ->select(DB::raw('stocks.quantity,products.id as id, products.price as price, products.name as product_name'))
            ->where([['categories.id',1]])->get();


        $soda = DB::table('stocks')
            ->join('products','stocks.product_id','=','products.id')
            ->join('categories','products.category_id','=','categories.id')
            ->select(DB::raw('stocks.quantity,products.id as id, products.price as price, products.name as product_name'))
            ->where([['categories.id',4]])->get();

        $wine  = DB::table('stocks')
            ->join('products','stocks.product_id','=','products.id')
            ->join('categories','products.category_id','=','categories.id')
            ->select(DB::raw('stocks.quantity,products.id as id, products.price as price, products.name as product_name'))
            ->where([['categories.id',2]])->get();


        $spirits  = DB::table('stocks')
            ->join('products','stocks.product_id','=','products.id')
            ->join('categories','products.category_id','=','categories.id')
            ->select(DB::raw('stocks.quantity,products.id as id, products.price as price, products.name as product_name'))
            ->where([['categories.id',3]])->get();


        $data_array = array(
            'beer'=>$Beer,
            'soda'=>$soda,
            'spirits'=>$spirits,
            'wine'=>$wine,
        );




        return view('admin/products',$data_array);
    }
    public function update(Request $request, $id)
    {
        //
        $product = Product::find($id);
        $product->price = $request->price;
        $product->save();
        return redirect('admin/products')->with('status','Price Updated Succesfully');
    }
        public function insert(Request $request){

            $business = new \App\Product();
            $business->name=$request->name;
            $business->price=$request->price;
            $business->category_id=$request->select;
            $business->save();

            $stock= new stock();
            $stock->product_id=$business->id;
            $stock->save();

            return redirect('admin/products')->with('status','Product Updated Succesfully');

        }



            public function stock (Request $request, $id)
            {
                //
                $product = $request->id;
                $quantity = $request->price;
                DB::table('stocks')->where('product_id','=',$product)->increment('quantity', $quantity);

                return redirect('admin/products')->with('status','Price Updated Succesfully');
            }

}

