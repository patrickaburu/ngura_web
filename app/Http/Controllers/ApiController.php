<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\stdClass;
use App\User;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use App\sms\AfricasTalkingGateway;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Product;
use App\Sale;
use App\Stock;

class ApiController extends Controller
{
    public function login(Request $request){
    	
        $username=$request->input('username');
        $password=$request->input('password');
         //$check_user=User::where([['email','=',$username],['password','=',$password]])->first();
        	$check_user=User::where('email','=',$username)->first();

         if(empty($check_user)){
           $result = array();
            $result['id'] = 0;
            return Response::json($result);
         }
         else{
         	if (Hash::check($password, $check_user->password)) {
                return Response::json($check_user);
        }
        else{
        	$result = array();
          $result['3'] = role;
         //   return Response::json("s" => "true".$check_user);
           return Response::json($result);
         }
        }
    }
    

///sending sms
    public function sms(Request $request){
        $recipients=$request->input('phone_number');
        $message=$request->input('message');

                      //  require_once('AfricasTalkingGateway.php');
            // Specify your login credentials
            $username   = "patrickaburu";
            $apikey     = "3d90354271f9be11e1104dbdb084363b564d037c036bd34ead0def6a1273e85b";
            // Specify the numbers that you want to send to in a comma-separated list
            // Please ensure you include the country code (+254 for Kenya in this case)
        //    $recipients = "+254723108293";
            // And of course we want our recipients to know what we really do
    //        $message    = "Evening Sir";
            // Create a new instance of our awesome gateway class
            $gateway    = new AfricasTalkingGateway($username, $apikey);
            // Any gateway error will be captured by our custom Exception class below, 
            // so wrap the call in a try-catch block
            try 
            { 
              // Thats it, hit send and we'll take care of the rest. 
              $results = $gateway->sendMessage($recipients, $message);
                        
              foreach($results as $result) {
                // status is either "Success" or "error message"
                echo " Number: " .$result->number;
                echo " Status: " .$result->status;
                echo " MessageId: " .$result->messageId;
                echo " Cost: "   .$result->cost."\n";
              }
            }
            catch ( AfricasTalkingGatewayException $e )
            {
              echo "Encountered an error while sending: ".$e->getMessage();
            }
            // DONE!!! 
    }

    //statements
    public  function statements(Request $request){
        try {
             $mytime = \Carbon\Carbon::today();

           $date= $mytime->toDateString();
           
          // $statement=Sales::where('sale_date','=',$date)->get();

           $results = DB::table('sales')
            ->join('products','sales.product_id','=','products.id')
            ->join('categories','products.category_id','=','categories.id')
            ->join('users','sales.users_id','=','users.id')
            ->select(DB::raw('products.name, SUM(sales.quantity) as quantity, SUM(sales.total) as total,users.name as User_name'))
            ->where([['sales.sale_date',$date],])
            //->select('products.name')
            ->groupBy('users.name')
            ->groupBy('sales.product_id')
            ->groupBy('products.name')
            ->orderBy('products.name','asc')->get();
           // return Response::json($results);
          
           
           if(empty($results)){
            return Response::json($date);
           }
           else{
           return Response::json($results);
            }
            
        } catch (Exception $exception) {
           return Response::json(array("success" => false,
                "message" => "" . $exception->getMessage())); 
        }
    }




  //total sales for specific days
  public function totalSale(Request $request){
    //$date=$request->input('date');
    try {
      	  $date=$request->input('date');
         // $mytime = \Carbon\Carbon::today();

         //   $date= $mytime->toDateString();

            $total = DB::table('sales')
             ->select(DB::raw('SUM(total) as amount'))
             ->where('sale_date',$date)->first();

return Response::json($total);
    	 }
          
        
     catch (Exception $e) {
        
    }
  } 


  //regiter cashier
   public function register(Request $request){
    try { 
    	$username=$request->input('username');
    	$name=$request->input('name');
      $pass=$request->input('password');
      $password=bcrypt($pass);
      $role=2;

    	$newuser = new User();
    	  $newuser->name = $name;
        $newuser->email = $username;
      $newuser->role= $role;
    	  $newuser->password = $password;

    	  	$newuser->Save();
          return Response::json($newuser);
    	  	// if(empty($newuser)){
    	  	// 	$result = array();
        //     $result['id'] = 0;
        //     return Response::json($result);

    	  	// }else
    	  	// {
    	  	// 	return Response::json($newuser);
    	  	// }
    	 } catch (Exception $e) {
        
    }
  }


//add a new product
  public function addproduct(Request $request){
    try { 
    $product_name=$request->input('name');
    $price=$request->input('price');
    $category=$request->input('pCategory');
    //$category=1;

    $newproduct=new Product();
    $newproduct->name=$product_name;
    $newproduct->price=$price;
    $newproduct->category_id=$category;
    $newproduct->save();

    $stock=New Stock();
    $stock->product_id=$newproduct->id;
    $stock->save();

    return Response::json($newproduct);
     } catch (Exception $e) {
        
    }
  } 

  //show remaining stock
  public function remainingStock(Request $request){
    try { 

        $stock = DB::table('stocks')
            ->join('products','stocks.product_id','=','products.id')
            ->join('categories','products.category_id','=','categories.id')
            ->select(DB::raw('products.name as name, stocks.quantity as quantity'))
            ->groupBy('products.name')
            ->groupBy('stocks.quantity')
            ->orderBy('stocks.quantity','asc')->get();

             return Response::json($stock);

      } catch (Exception $e) {
        
    }}
   
}