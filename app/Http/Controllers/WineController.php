<?php

namespace App\Http\Controllers;

use App\Products;
use App\Sale;
use App\User;
use carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\ Beer;


use Input;
use DB;

class WineController extends Controller
{
    public function index(){
        return view('wine');
    }

    public function  view(){
        $wine = DB::table('products')->where('category_id', '=', 2)->get();


//    return view('beer')->with('data',$Beer);
        return view('wine',['data'=>$wine]);


    }
    public function ajax(){

        $cat_id=input::get('cat_id');
        $subcategories = Beer::where('id', '=' ,$cat_id)->get();
        return ($subcategories);


    }

    public function create(Request $request ){

        $this->validate($request, [
            'quantity' => 'required',

        ]);

        $price=$request->price;
        $quantity=$request->quantity;
        $product_id=$request->product;
      //  echo $quantity;

       $confirm= DB::table('stocks')->where('product_id','=',$product_id)->first();
        $conn=$confirm->quantity;


        if( $conn > $quantity){

        $mytime = \Carbon\Carbon::today();

        $date= $mytime->toDateString();

        $price=$request->price;
        $quantity=$request->quantity;
        $product_id=$request->product;


        $sales = new Sale();
        $sales->quantity=$request->quantity;
        $sales->product_id=$request->product;
        $sales->sale_date=$date;
        $sales->users_id=Auth::id();
        $sales->total=$quantity*$price;

        $sales->save();


        DB::table('stocks')->where('product_id','=',$product_id)->decrement('quantity', $quantity);

        return redirect('view')->with('status','saved successfully');
        }
        else{
            return redirect('view')->with('status','Out of stock!');

        }

    }
}