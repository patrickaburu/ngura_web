<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Sale;
use DB;
use Illuminate\Support\Facades\Response;
use carbon;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function report(){
        $mytime = \Carbon\Carbon::today();

        $date= $mytime->toDateString();

        $results = DB::table('sales')
            ->join('products','sales.product_id','=','products.id')
            ->join('categories','products.category_id','=','categories.id')
            ->join('users','sales.users_id','=','users.id')
            ->select(DB::raw('products.name, SUM(sales.quantity) as quantity, SUM(sales.total) as total,users.name as User_name'))
            ->where([['sales.sale_date',$date], ['sales.users_id',Auth::id()], ])

            ->groupBy('users.name')
            ->groupBy('sales.product_id')
            ->groupBy('products.name')->get();

//        $total=DB::table('sales')
        $report = DB::table('sales')->where([
            ['users_id', '=', Auth::id()],
            ['sale_date', '=', $date],
        ])->get();




         $sum=$report->sum('total');

        return view('report',['data'=>$results] ,['total'=>$sum]);


    }


    public function mash(){
        $mytime = \Carbon\Carbon::today();

        $date= $mytime->toDateString();


        $results = DB::table('sales')
            ->join('products','sales.product_id','=','products.id')
            ->join('categories','products.category_id','=','categories.id')
            ->join('users','sales.users_id','=','users.id')
            ->select(DB::raw('products.name, SUM(sales.quantity) as quantity, SUM(sales.total) as total,users.name as User_name'))
            ->where([['sales.sale_date','6'], ['sales.users_id','1'], ])

            ->groupBy('users.name')
            ->groupBy('sales.product_id')
            ->groupBy('products.name')->get();
//        return Response::json($results);

        return view('repo',['data'=>$results] );


    }
}
